﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            
            ViewBag.Message =  DateTime.Now.ToString();
            List<string> SupervisorInfo = new List<string>();
            foreach (Supervisor c in SupervisorGroup.GetSampleSupervisorData())
            {
                int? id = c?.Id ?? 8;
                string name = c?.Name ?? "no name";
                string level = c?.Level ?? "no level";
                bool? available = c?.IsAvailable ?? false;
              SupervisorInfo.Add($"ID:{id} Name:{name} Level:{level} Available: {available} ");
               //SupervisorInfo.Add($"{nameof (c.Id)}:{c.Id} {nameof(c.Name)}:{c.Name} {nameof(c.Level)}:{c.Level} {nameof(c.IsAvailable)}:{c.IsAvailable} ");
            }
            return View("MyFirstView", SupervisorInfo);
            
        }
        
        public IActionResult SupervisorListStartWithS()
        {
            // Find the supervisor name which are start with "s"
            return View(SupervisorGroup.GetSampleSupervisorData().Where(n => n.Name.ToLower().StartsWith("s")).Select(n => n.Name));
        }

        [HttpGet]
        public IActionResult SupervisorInfo()
        {
            return View();
        }
        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)

        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.Supervisors.Add(supervisor);
                return View("AddSupervisorConfirmation", supervisor);
            }
            else
            {
                return View();
            } 
        }
        [HttpGet]
        public IActionResult AllSupervisors()

        {
            return View(SupervisorGroup.Supervisors);
        }

    }
}
