﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    public class Supervisor
    {
        [Required(ErrorMessage = "Please Enter a number ")]
        [RegularExpression("^\\d+$", ErrorMessage = "Please Enter valid whole number ")]
        public int? Id { get; set; } = 123;
        [Required(ErrorMessage = "Please Enter a name ")]
        public string Name { get; set; } = "Nameless";
        [Required(ErrorMessage = "Please indicate availablity")]
        public bool? IsAvailable { get; set; } = false;
        [Required(ErrorMessage = "Please Enter a competence level ")]
        public string Level { get; set; } = "none";
    }
}
