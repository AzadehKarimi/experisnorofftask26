﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    public static class SupervisorGroup
    {
        private static List<Supervisor> CurrentSupervisors = new List<Supervisor>();
        public static List<Supervisor> Supervisors
        {
            get { return CurrentSupervisors; }
        }
        public static void AddSupervisor(Supervisor newsupervisor)
        {
            CurrentSupervisors.Add(newsupervisor);
        }
        public static List<Supervisor> GetSampleSupervisorData()
        {
            Supervisor supervisor1 = new Supervisor { Id = 1, IsAvailable = true,Level="Pro",Name="Jack" };
            Supervisor supervisor2 = new Supervisor { Id = 2, IsAvailable = false, Level = "Pro", Name = "Samanta" };
            Supervisor supervisor3 = new Supervisor { Id = 2, IsAvailable = false, Level = "Pro", Name = "Sara" };
            Supervisor supervisor4 = new Supervisor { };
            List<Supervisor> supervisors = new List<Supervisor>();
            supervisors.Add(supervisor1);
            supervisors.Add(supervisor2);
            supervisors.Add(supervisor3);
            supervisors.Add(supervisor4);
            return supervisors;

        }
    }
}
